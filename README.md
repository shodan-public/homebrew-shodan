# Homebrew tap repository for various Shodan tools


## Installation ([available packages](https://gitlab.com/shodan-public/homebrew-shodan/-/tree/main/Formula))

```shell
brew tap shodan-public/homebrew-shodan https://gitlab.com/shodan-public/homebrew-shodan
brew install strend
```

## Add new formula

* Create a new **Formula/new-formula.rb**

* Build and test

```bash
HOMEBREW_NO_INSTALL_FROM_API=1 brew install --build-from-source Formula/new-formula.rb
brew test Formula/new-formula.rb
brew audit --strict --online new-formula    # Only work if formula installed from tap repo
```

## Troubleshooting

If there are any conflicts when upgrade a package, could be git upstream commits failed to merge. Untap the repo and reinstall it.

```shell
brew untap shodan-public/homebrew-shodan
brew tap shodan-public/homebrew-shodan https://gitlab.com/shodan-public/homebrew-shodan
brew reinstall strend
```

## TODO

Publish the package to [homebrew-core](https://github.com/Homebrew/homebrew-core)