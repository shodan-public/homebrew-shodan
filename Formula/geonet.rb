class Geonet < Formula
  desc "Geographic Network Tools"
  homepage "https://gitlab.com/shodan-public/geonet-rs"
  url "https://gitlab.com/api/v4/projects/32089582/packages/generic/geonet-rs/latest/geonet_latest_darwin_x86_64.tar.gz"
  version "0.4.4"
  sha256 "307078b0897cec12af64a14cc96a6e372208f206614654db10387b2f2a3cdc75"
  license "MIT"

  on_macos do
    on_arm do
      url "https://gitlab.com/api/v4/projects/32089582/packages/generic/geonet-rs/latest/geonet_latest_darwin_arm64.tar.gz"
      sha256 "d734860755c7e1f0d6a26019f8095549104b7190a1617f39ad8b53a838444968"
    end

    def install
      # Correct download url, amd64 is same as x86_64
      arch = Hardware::CPU.arch
      arch = "x86_64" if arch.to_s == "amd64"

      bin.install "geodns-#{version}-darwin-#{arch}" => "geodns"
      bin.install "geoping-#{version}-darwin-#{arch}" => "geoping"
    end
  end

  test do
    system "#{bin}/geodns", "google.com"
    system "#{bin}/geoping", "8.8.8.8"
  end
end
