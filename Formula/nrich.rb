class Nrich < Formula
  desc "IPs Analyze Tool"
  homepage "https://gitlab.com/shodan-public/nrich"
  url "https://gitlab.com/api/v4/projects/33695681/packages/generic/nrich/latest/nrich_latest_darwin_x86_64.tar.gz"
  version "0.4.2"
  sha256 "976a70d4d34550689b00b26e4b39efb4d745f79b768ee9ea479a7c9fde885af7"
  license "MIT"

  on_macos do
    on_arm do
      url "https://gitlab.com/api/v4/projects/33695681/packages/generic/nrich/latest/nrich_latest_darwin_arm64.tar.gz"
      sha256 "82ec8a6c0324e11c9c8694963fdc38a3e0157ed21dfe174879ea52fdcf54bd6a"
    end

    def install
      # Correct download url, amd64 is same as x86_64
      arch = Hardware::CPU.arch
      arch = "x86_64" if arch.to_s == "amd64"

      bin.install "nrich-#{version}-darwin-#{arch}" => "nrich"
    end
  end

  test do
    system "echo", "149.202.182.140", "|", "#{bin}/nrich", "-"
  end
end
