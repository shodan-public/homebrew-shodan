class Strend < Formula
  desc "Search and visualize Shodan historical data in the terminal"
  homepage "https://gitlab.com/shodan-public/strend"
  url "https://gitlab.com/api/v4/projects/52410656/packages/generic/strend/latest/strend-latest-darwin-x86_64.tar.gz"
  version "0.2.2"
  sha256 "7c810e0322541dfc40e15ce0d2fc670487fd7b07029272d5ab2fa006fb5ac0ed"
  license "MIT"

  on_macos do
    on_arm do
      url "https://gitlab.com/api/v4/projects/52410656/packages/generic/strend/latest/strend-latest-darwin-arm64.tar.gz"
      sha256 "90b19c6e0651b5fe44461565287715657caefd07d1c861ff8fe14a591cd2b863"
    end

    def install
      # Correct download url, amd64 is same as x86_64
      arch = Hardware::CPU.arch
      arch = "x86_64" if arch.to_s == "amd64"

      bin.install "strend-#{version}-darwin-#{arch}" => "strend"
    end
  end

  test do
    assert_match version.to_s, shell_output("#{bin}/strend --version")
    assert_match "Error: Invalid arguments, please check: strend --help",
      shell_output("#{bin}/strend --query '' --facets os", 1)
  end
end
